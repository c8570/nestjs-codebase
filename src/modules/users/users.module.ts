import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { UserRepository } from './users.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './entities/user.entity';
import { UserRolesModule } from '../user-roles/user-roles.module';

@Module({
	imports: [
		MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
		UserRolesModule,
	],
	controllers: [UsersController],
	providers: [UsersService, UserRepository],
})
export class UsersModule {}
