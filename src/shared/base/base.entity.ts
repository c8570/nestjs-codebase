import { Prop, Schema } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import { ObjectId } from 'mongoose';

@Schema({
	timestamps: {
		createdAt: 'created_at',
		updatedAt: 'updated_at',
	},
	versionKey: false,
})
export class BaseEntity {
	@Transform(({ value }) => value.toString())
	_id: ObjectId;

	@Prop({ type: Date, default: new Date() })
	created_at: Date;

	@Prop({ type: Date, default: new Date() })
	updated_at: Date;

	@Prop({ type: Date, default: null })
	deleted_at: Date;
}
