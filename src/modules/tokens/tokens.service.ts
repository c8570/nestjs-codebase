import { Injectable, Logger } from '@nestjs/common';
import { BaseService } from 'src/shared';
import { Token } from './entities/token.entity';
import { TokenRepository } from './tokens.repository';

@Injectable()
export class TokensService extends BaseService<Token> {
	constructor(private readonly token_repository: TokenRepository) {
		super(token_repository, new Logger(TokensService.name));
	}
}
