import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseRepository } from 'src/shared';
import { TokenDocument, Token } from './entities/token.entity';

@Injectable()
export class TokenRepository extends BaseRepository<Token> {
	constructor(
		@InjectModel(Token.name)
		private readonly token_model: Model<TokenDocument>,
	) {
		super(token_model);
	}
}
