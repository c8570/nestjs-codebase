import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import * as Joi from 'joi';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TokensModule } from './modules/tokens/tokens.module';
import { UserRolesModule } from './modules/user-roles/user-roles.module';
import { UsersModule } from './modules/users/users.module';

@Module({
	imports: [
		ConfigModule.forRoot({
			validationSchema: Joi.object({
				SERVICE_PORT: Joi.number().required(),
				EXPOSE_PORT: Joi.number().required(),
			}),
		}),
		MongooseModule.forRootAsync({
			imports: [ConfigModule],
			useFactory: async (config_service: ConfigService) => ({
				uri: `${config_service.get<string>(
					'DATABASE_URI',
				)}/${config_service.get(
					'DATABASE_NAME',
				)}?authSource=admin&directConnection=true&retryWrites=true`,
			}),
			inject: [ConfigService],
		}),
		UserRolesModule,
		UsersModule,
		TokensModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
