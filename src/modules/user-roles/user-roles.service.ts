import { Injectable, Logger } from '@nestjs/common';
import { BaseService } from 'src/shared';
import { UserRole } from './entities/user-role.entity';
import { UserRolesRepository } from './user-roles.repository';

@Injectable()
export class UserRolesService extends BaseService<UserRole> {
	constructor(private readonly user_role_repository: UserRolesRepository) {
		super(user_role_repository, new Logger(UserRolesService.name));
	}
}
