import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { NextFunction } from 'express';
import * as mongoose from 'mongoose';
import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'src/shared';

export type TokenDocument = mongoose.HydratedDocument<Token>;
@Schema()
export class Token extends BaseEntity {
	@Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
	user: User;

	@Prop({ type: String, required: true })
	access_token: string;

	@Prop({ type: String, required: true })
	refresh_token: string;

	@Prop({ type: [String], required: true })
	devices: string[];
}

const schema = SchemaFactory.createForClass(Token);

schema.pre<TokenDocument>('save', function (next: NextFunction) {
	const now = new Date();
	if (!this.created_at) this.created_at = now;
	next();
});

schema.pre<TokenDocument>('updateOne', function (next: NextFunction) {
	const now = new Date();
	if (!this.updated_at) this.updated_at = now;
	next();
});

export const TokenSchema = schema;
