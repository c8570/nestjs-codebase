import {
	Controller,
	Get,
	Post,
	Body,
	Patch,
	Param,
	Delete,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags(UsersController.name)
@Controller('users')
export class UsersController {
	constructor(private readonly users_service: UsersService) {}

	@Post()
	async create(@Body() create_user_dto: CreateUserDto) {
		return await this.users_service.create(create_user_dto);
	}

	@Get()
	findAll() {
		return this.users_service.findAll({}, ['role']);
	}

	@Get(':id')
	findOne(@Param('id') _id: string) {
		return this.users_service.findOne(_id);
	}

	@Patch(':id')
	update(@Param('id') _id: string, @Body() update_user_dto: UpdateUserDto) {
		return this.users_service.update(_id, update_user_dto);
	}

	@Delete(':id')
	remove(@Param('id') _id: string) {
		return this.users_service.remove(_id);
	}
}
