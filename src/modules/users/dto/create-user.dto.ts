import { ApiProperty } from '@nestjs/swagger';
import {
	IsNotEmpty,
	IsEmail,
	IsEnum,
	MinLength,
	MaxLength,
} from 'class-validator';
import { USER_ROLES } from 'src/modules/user-roles/entities/user-role.entity';
import { GENDER } from '../entities/user.entity';

export class CreateUserDto {
	@ApiProperty({
		type: String,
		example: 'John',
	})
	@IsNotEmpty()
	first_name: string;

	@ApiProperty({
		type: String,
		example: 'Doe',
	})
	@IsNotEmpty()
	last_name: string;

	@ApiProperty({
		type: String,
		example: 'johndoe@gmail.com',
	})
	@IsEmail()
	@IsNotEmpty()
	email: string;

	@ApiProperty({
		type: String,
		example: GENDER.MALE,
	})
	@IsEnum(GENDER)
	gender?: GENDER;

	@ApiProperty({
		type: String,
		example: USER_ROLES.STUDENTS,
	})
	@IsEnum(USER_ROLES)
	role?: USER_ROLES;

	@ApiProperty({
		type: String,
		example: 'johndoe@123',
	})
	@IsNotEmpty()
	@MinLength(8)
	@MaxLength(60)
	password: string;
}
