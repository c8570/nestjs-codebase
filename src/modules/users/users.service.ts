import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { BaseService } from 'src/shared';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import { UserRepository } from './users.repository';
/** USER ROLES */
import { USER_ROLES } from '../user-roles/entities/user-role.entity';
import { UserRolesService } from '../user-roles/user-roles.service';

@Injectable()
export class UsersService extends BaseService<User> {
	constructor(
		private readonly users_repository: UserRepository,
		private readonly user_role: UserRolesService,
	) {
		super(users_repository, new Logger(UsersService.name));
	}

	async create(create_dto: CreateUserDto): Promise<User> {
		try {
			if (!create_dto.role) {
				return await this.users_repository.create({
					...create_dto,
					role: USER_ROLES.STUDENTS,
				});
			}
			const valid_role = await this.user_role.findOneByCondtions({
				name: create_dto.role,
			});
			if (!valid_role) {
				throw new BadRequestException('Valid role');
			}
			return await this.users_repository.create({
				...create_dto,
				role: valid_role._id,
			});
		} catch (error) {
			this.logger.error(error);
			return error;
		}
	}
}
