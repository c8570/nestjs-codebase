import {
	Controller,
	Get,
	Post,
	Body,
	Patch,
	Param,
	Delete,
} from '@nestjs/common';
import { TokensService } from './tokens.service';
import { CreateTokenDto } from './dto/create-token.dto';
import { UpdateTokenDto } from './dto/update-token.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags(TokensController.name)
@Controller('tokens')
export class TokensController {
	constructor(private readonly tokens_service: TokensService) {}

	@Post()
	create(@Body() create_token_dto: CreateTokenDto) {
		return this.tokens_service.create(create_token_dto);
	}

	@Get()
	findAll() {
		return this.tokens_service.findAll();
	}

	@Get(':id')
	findOne(@Param('id') _id: string) {
		return this.tokens_service.findOne(_id);
	}

	@Patch(':id')
	update(@Param('id') _id: string, @Body() update_token_dto: UpdateTokenDto) {
		return this.tokens_service.update(_id, update_token_dto);
	}

	@Delete(':id')
	remove(@Param('id') _id: string) {
		return this.tokens_service.remove(_id);
	}
}
