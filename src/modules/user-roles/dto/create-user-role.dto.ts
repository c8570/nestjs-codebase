import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { USER_ROLES } from '../entities/user-role.entity';

export class CreateUserRoleDto {
	@ApiProperty({
		type: String,
		example: USER_ROLES.ADMIN,
	})
	@IsNotEmpty()
	name: USER_ROLES;

	@ApiProperty({
		type: String,
		example: 'Administrator',
	})
	@IsOptional()
	description?: string;
}
