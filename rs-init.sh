#!/bin/bash

mongosh <<EOF
var config = {
    "_id": "dbrs",
    "version": 1,
    "members": [
        {
            "_id": 1,
            "host": "tu_learning_mongo_primary:27017",
            "priority": 3
        },
        {
            "_id": 2,
            "host": "tu_learning_mongo_secondary_1:27017",
            "priority": 2
        },
        {
            "_id": 3,
            "host": "tu_learning_mongo_secondary_2:27017",
            "priority": 1
        }
    ]
};
rs.initiate(config, { force: true });
rs.status();
EOF
sleep 15
mongosh <<EOF
use admin;
db.createUser({ user: '$MONGO_INITDB_ROOT_USERNAME', pwd: '$MONGO_INITDB_ROOT_PASSWORD', roles: [{role: 'root', db:'admin'}]});
EOF
