import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import helmet from 'helmet';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	const logger = new Logger(bootstrap.name);
	app.useGlobalPipes(new ValidationPipe());
	const config_service = app.get(ConfigService);
	// Swagger config
	const config = new DocumentBuilder()
		.setTitle('Synchronous API')
		.setDescription('The Synchronous API description for LMS system')
		.addServer(
			config_service.get('NODE_ENV') === 'development'
				? `http://localhost:${config_service.get('EXPOSE_PORT')}`
				: config_service.get('API_URL'),
			'MAIN',
		)
		.setVersion('1.0')
		.build();
	const document = SwaggerModule.createDocument(app, config);
	SwaggerModule.setup('api', app, document);
	app.use(helmet());
	console.log(process.env.NODE_ENV);
	await app.listen(config_service.get('SERVICE_PORT'), () =>
		logger.log(
			`Starting server successfully: ${config_service.get(
				'SERVICE_PORT',
			)} and running on port ${config_service.get('EXPOSE_PORT')}`,
		),
	);
}
bootstrap();
