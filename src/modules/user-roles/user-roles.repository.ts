import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseRepository } from 'src/shared';
import { UserRole, UserRoleDocument } from './entities/user-role.entity';

@Injectable()
export class UserRolesRepository extends BaseRepository<UserRole> {
	constructor(
		@InjectModel(UserRole.name)
		private readonly user_role_model: Model<UserRoleDocument>,
	) {
		super(user_role_model);
	}
}
