import { Logger } from '@nestjs/common';
import { BaseRepository } from './base.repository';

export class BaseService<T> {
	constructor(
		private repository: BaseRepository<T>,
		protected readonly logger: Logger,
	) {}
	async create(create_dto): Promise<T> {
		try {
			return await this.repository.create(create_dto);
		} catch (error) {
			this.logger.error(error);
		}
	}

	async findAll(
		filter = {},
		populate = [],
	): Promise<{ count: number; items: T[] }> {
		return await this.repository.findAll({ filter, populate });
	}

	async findOne(_id: string): Promise<T> {
		return await this.repository.findOne(_id);
	}

	async findOneByCondtions(filter = {}): Promise<T> {
		return await this.repository.findOneByCondtions(filter);
	}

	async update<F>(_id: string, update_dto: F): Promise<T> {
		return await this.repository.update<F>(_id, update_dto);
	}

	async remove(_id: string): Promise<T> {
		return await this.repository.remove(_id);
	}
}
