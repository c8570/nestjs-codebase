import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Exclude } from 'class-transformer';
import { NextFunction } from 'express';
import * as mongoose from 'mongoose';
import { UserRole } from 'src/modules/user-roles/entities/user-role.entity';
import { BaseEntity } from 'src/shared';

export type UserDocument = mongoose.HydratedDocument<User>;

export enum GENDER {
	MALE = 'MALE',
	FEMALE = 'FEMALE',
	OTHER = 'OTHER',
}

@Schema()
export class User extends BaseEntity {
	@Prop({ type: Number, auto: true })
	friendly_id?: number;

	@Prop({ type: String, required: true })
	first_name: string;

	@Prop({ type: String, required: true })
	last_name: string;

	@Prop({ type: String })
	headline?: string;

	@Prop({ type: String, required: true, unique: true })
	email: string;

	@Prop({ type: String, enum: GENDER })
	gender?: GENDER;

	@Prop({ type: Date })
	dob?: Date;

	@Prop({ type: String })
	@Exclude()
	password?: string;

	@Prop({ type: String })
	phonenumber?: string;

	@Prop({ type: String })
	address?: string;

	@Prop({
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: UserRole.name,
	})
	role: UserRole;

	@Prop({ type: String })
	avatar?: string;

	@Prop({ type: Boolean, required: true, default: false })
	is_email_verified?: boolean;

	@Prop({ type: String })
	last_email_confirmation_link?: string;

	@Prop({ type: Boolean, required: true, default: false })
	is_phone_verified?: boolean;

	@Prop({ type: Number, required: true, default: 0 })
	point: number;

	@Prop({ type: Date, required: true, default: new Date() })
	last_activity?: string;

	@Prop({ type: Boolean, required: true, default: false })
	is_tutor: boolean;
}

const schema = SchemaFactory.createForClass(User);

schema.pre<UserDocument>('save', function (next: NextFunction) {
	const now = new Date();
	if (!this.created_at) this.created_at = now;
	next();
});

schema.pre<UserDocument>('updateOne', function (next: NextFunction) {
	const now = new Date();
	if (!this.updated_at) this.updated_at = now;
	next();
});

export const UserSchema = schema;
