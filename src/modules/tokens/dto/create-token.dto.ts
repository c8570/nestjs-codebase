import { IsNotEmpty } from 'class-validator';

export class CreateTokenDto {
	@IsNotEmpty()
	access_token: string;

	@IsNotEmpty()
	refresh_token: string;

	@IsNotEmpty()
	devices: string[];
}
