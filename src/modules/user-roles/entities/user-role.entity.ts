import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { NextFunction } from 'express';
import * as mongoose from 'mongoose';
import { BaseEntity } from 'src/shared';

export type UserRoleDocument = mongoose.HydratedDocument<UserRole>;

export enum USER_ROLES {
	ADMIN = 'ADMIN',
	MANAGER = 'MANAGER',
	STUDENTS = 'STUDENTS',
}

@Schema()
export class UserRole extends BaseEntity {
	@Prop({ type: String, required: true, enum: USER_ROLES })
	name: USER_ROLES;

	@Prop({ type: String })
	description?: string;
}

const schema = SchemaFactory.createForClass(UserRole);

schema.pre<UserRoleDocument>('save', function (next: NextFunction) {
	const now = new Date();
	if (!this.created_at) this.created_at = now;
	next();
});

schema.pre<UserRoleDocument>('updateOne', function (next: NextFunction) {
	const now = new Date();
	if (!this.updated_at) this.updated_at = now;
	next();
});

export const UserRoleSchema = schema;
