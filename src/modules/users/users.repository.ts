import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseRepository } from 'src/shared';
import { User, UserDocument } from './entities/user.entity';

@Injectable()
export class UserRepository extends BaseRepository<User> {
	constructor(
		@InjectModel(User.name)
		private readonly user_repository: Model<UserDocument>,
	) {
		super(user_repository);
	}
}
