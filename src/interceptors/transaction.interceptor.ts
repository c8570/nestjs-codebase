import {
	CallHandler,
	ExecutionContext,
	Injectable,
	NestInterceptor,
} from '@nestjs/common';
import { catchError, Observable, tap } from 'rxjs';
import * as mongoose from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';
@Injectable()
export class TransactionInterceptor implements NestInterceptor {
	constructor(
		@InjectConnection() private readonly connection: mongoose.Connection,
	) {}
	async intercept(
		context: ExecutionContext,
		next: CallHandler<any>,
	): Promise<Observable<any>> {
		console.log('Init transaction');
		const request = context.switchToHttp().getRequest();
		const session: mongoose.ClientSession =
			await this.connection.startSession();
		request.mongooseSession = session;
		session.startTransaction();
		return next.handle().pipe(
			tap(async () => {
				await session.commitTransaction();
				session.endSession();
			}),
			catchError(async (err) => {
				await session.abortTransaction();
				session.endSession();
				throw err;
			}),
		);
	}
}
