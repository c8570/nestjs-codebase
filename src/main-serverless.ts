import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import helmet from 'helmet';
import { AppModule } from './app.module';
import serverlessExpress from '@vendia/serverless-express';
import { Logger } from '@nestjs/common';
import { Callback, Context, Handler } from 'aws-lambda';

let server: Handler;

async function bootstrap(): Promise<Handler> {
	const app = await NestFactory.create(AppModule);
	await app.init();

	const express_app = app.getHttpAdapter().getInstance();
	return serverlessExpress({ app: express_app });
}

export const handler: Handler = async (
	event: any,
	context: Context,
	callback: Callback,
) => {
	server = server ?? (await bootstrap());
	return server(event, context, callback);
};
// async function bootstrap() {
// 	const app = await NestFactory.create(AppModule);
// 	const logger = new Logger(bootstrap.name);
// 	const config_service = app.get(ConfigService);
// 	// Swagger config
// 	const config = new DocumentBuilder()
// 		.setTitle('Synchronous API')
// 		.setDescription('The Synchronous API description for LMS system')
// 		.addServer(
// 			config_service.get('NODE_ENV') === 'development'
// 				? `http://localhost:${config_service.get('EXPOSE_PORT')}`
// 				: config_service.get('API_URL'),
// 			'MAIN',
// 		)
// 		.setVersion('1.0')
// 		.build();
// 	const document = SwaggerModule.createDocument(app, config);
// 	SwaggerModule.setup('api', app, document);
// 	app.use(helmet());
// 	console.log(process.env.NODE_ENV);
// 	await app.listen(config_service.get('SERVICE_PORT'), () =>
// 		logger.log(
// 			`Starting server successfully: ${config_service.get(
// 				'SERVICE_PORT',
// 			)} and running on port ${config_service.get('EXPOSE_PORT')}`,
// 		),
// 	);
// }
// bootstrap();
