import { Model } from 'mongoose';

export class BaseRepository<T> {
	constructor(private model: Model<T>) {}
	async create<F>(dto: F) {
		return await this.model.create(dto);
	}

	async findAll({ filter = {}, populate = [] }) {
		const items = await this.model.find({ ...filter }).populate(populate);
		return {
			count: items.length,
			items,
		};
	}

	async findOne(_id: string) {
		return await this.model.findById(_id);
	}

	async findOneByCondtions(filter = {}) {
		return await this.model.findOne({ ...filter });
	}

	async update<F>(_id: string, dto: F) {
		return await this.model.findByIdAndUpdate(_id, dto);
	}

	async remove(_id: string) {
		return await this.model.findByIdAndUpdate(_id, {
			is_deleted: new Date(),
		});
	}
}
